const env = require("../../.env");

exports.pusher = {
	appId: env.PUSHER_ID,
	key: env.PUSHER_KEY,
	secret: env.PUSHER_SECRET,
	cluster: env.PUSHER_CLUSTER,
	encrypted: env.PUSHER_ENCRYPTED,
};
