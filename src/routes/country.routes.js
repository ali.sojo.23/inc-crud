const express = require("express");
const Country = require("../controllers/country/country.controller");
const router = express.Router();

router.post("/", Country.POST);
router.get("/:id", Country.GetById);
router.get("/", Country.GET);

module.exports = router;
