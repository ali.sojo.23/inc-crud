const express = require("express");
const router = express.Router();
const Users = require("../controllers/auth/auth.controller");

router.post("/register", Users.createUser);
router.post("/login", Users.loginUser);
router.post("/forgot/email", Users.sendEmailToResetPassword);
router.post("/verify/resend/:id", Users.resendEmail);
router.get("/verify/:tkn", Users.verifyEmail);

module.exports = router;
