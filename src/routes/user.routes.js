const express = require("express");
const Users = require("../controllers/Users/users.controller");
const router = express.Router();

router.get("/", Users.GET);
router.put("/", Users.GetByEmail);
router.post("/:id", Users.POST);
router.get("/:id", Users.GetByAuthId);
router.post("/edit/:id", Users.PUT);
router.post("/avatar/:id", Users.SetAvatarToUser);

module.exports = router;
