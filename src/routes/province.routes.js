const express = require("express");
const Provinces = require("../controllers/provinces/provinces.controllers");
const router = express.Router();

router.post("/", Provinces.POST);
router.get("/:id", Provinces.FIND);
router.get("/country/:id", Provinces.findByCountry);
router.get("/", Provinces.GET);

module.exports = router;
