const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const infoSchema = new schema(
	{
		Country: {
			type: schema.Types.ObjectId,
			ref: "Country",
		},
		Province: {
			type: schema.Types.ObjectId,
			ref: "Provinces",
		},
		Address: {
			type: String,
		},
		Birthdate: {
			type: String,
			trim: true,
		},
		Gender: {
			type: String,
			trim: true,
		},
		Phone: {
			type: String,
			trim: true,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Info", infoSchema);
