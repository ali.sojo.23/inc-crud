const Studies = require("../../models/studies.model");

exports.POST = async (req, res) => {
	const newStudy = {
		Institute: req.body.Institute,
		Grade: req.body.Grade,
		Subject: req.body.Subject,
		Start: req.body.Start,
		End: req.body.End,
		User: req.body.User,
	};
	const study = await new Studies(newStudy).save();

	if (study) {
		res.send({
			message: "saved",
			code: 200,
		});
	}
};

exports.GET = async (req, res) => {
	const studies = await Studies.find();

	res.send({
		code: 200,
		studies,
	});
};

exports.findById = async (req, res) => {
	const { id } = req.params;

	const studies = await Studies.find({ User: id });

	res.send({
		code: 200,
		studies: studies,
	});
};
