const mongoose = require("mongoose");
const authSchema = require("../../models/auth.model");

authSchema.statics = {
	create: async function(data, cb) {
		const user = new this(data);
		await user.save(cb);
	},
	login: async function(query, cb) {
		await this.find(query, cb);
	},
};

const authModel = mongoose.model("Auth", authSchema);
module.exports = authModel;
